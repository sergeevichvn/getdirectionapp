package com.vnazarov.getdestinationapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import static android.os.Build.VERSION.SDK_INT;
import static com.vnazarov.getdestinationapp.Utils.*;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, ActivityCompat.OnRequestPermissionsResultCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final int PERMISSION_REQUEST_ID = 777;
    private static final LatLng TARGET_MARKER = new LatLng(55.778914190059794, 37.59541869163513);

    private String NO_PERMISSON_GPS;
    private String ERROR_GET_DISTANCE;
    private String DISTANCE_LANGUAGE;
    private String API_KEY;
    private String TRAVEL_MODE;
    private String GOOGLE_CLIENT_ERROR;
    private String GPS_TURN_OFF;

    private GoogleMap googleMap;
    private GoogleApiClient googleClient;
    private Location location;
    private LocationRequest locationRequest;
    private LatLng device;
    private boolean havePermissions = false;

    private ProgressBar progressBar;
    private TextView distanceView;
    private TextView durationView;
    private Button getDirectionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        init();
    }

    //Инициализируем переменные
    private void init() {
        NO_PERMISSON_GPS = getString(R.string.no_permission_gps);
        ERROR_GET_DISTANCE = getString(R.string.error_get_distance);
        DISTANCE_LANGUAGE = getString(R.string.distance_language);
        API_KEY = getString(R.string.google_maps_key);
        TRAVEL_MODE = getString(R.string.travel_mode);
        GOOGLE_CLIENT_ERROR = getString(R.string.google_client_error);
        GPS_TURN_OFF = getString(R.string.gps_turn_off);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        distanceView = (TextView) findViewById(R.id.distance);
        durationView = (TextView) findViewById(R.id.duration);
        getDirectionButton = (Button) findViewById(R.id.getdirections);

        distanceView.setVisibility(View.INVISIBLE);
        durationView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        getDirectionButton.setEnabled(false);

        googleClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        locationRequest = new LocationRequest()
                .setInterval(5000)
                .setFastestInterval(5000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    //Проверяем имеим ли мы разрешение и если нет то заправливаем для Android > 23 API
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_ID);
        } else {
            havePermissions = true;
        }
    }

    public void getDirection(View v) {
        Uri targeLocation = Uri.parse("google.navigation:q=" + TARGET_MARKER.latitude + "," + TARGET_MARKER.longitude);
        Intent googleNavigator = new Intent(Intent.ACTION_VIEW, targeLocation);
        googleNavigator.setPackage("com.google.android.apps.maps");
        startActivity(googleNavigator);
    }

    //Что ответил пользователь на запрос на разрешение
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_ID) {
            if (grantResults.length == 1
                    && permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                havePermissions = true;
            }
        }
    }

    private void doMagic() {
        device = new LatLng(location.getLatitude(), location.getLongitude());
        googleMap.addMarker(new MarkerOptions().position(device).title("You").icon(getIconFromResource(this, R.drawable.ic_pin)));
        googleMap.addMarker(new MarkerOptions().position(TARGET_MARKER).title("Target").icon(getIconFromResource(this, R.drawable.ic_info)));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(device));
        getDirectionButton.setEnabled(true);
        new GetDistance().execute();
    }

    //Когда googleClient готов, нужно проверить разрешения и подписаться на обновление локации
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        }
        if (havePermissions) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleClient, locationRequest, this);
            checkGPS();
        } else {
            showMessage(this, NO_PERMISSON_GPS);
        }
    }

    private void checkGPS(){
        if(!getGPSStatus(this))
            showMessage(this, GPS_TURN_OFF);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onStart() {
        googleClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleClient.disconnect();
        super.onStop();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        showMessage(this, GOOGLE_CLIENT_ERROR);
    }

    //Вызывается, когда мы получили какой-то location от LocationServices
    @Override
    public void onLocationChanged(Location location) {
        if(location != null){
            this.location = location;
            LocationServices.FusedLocationApi.removeLocationUpdates(googleClient, this);
            doMagic();
        }
    }


    private class GetDistance extends AsyncTask<Void, Void, String> {

        private String distance;
        private String duration;

        private HttpUrl getURL() {
            String destinations = device.latitude + "," + device.longitude + "|"
                    + TARGET_MARKER.latitude + "," + TARGET_MARKER.longitude;
            return new HttpUrl.Builder()
                    .scheme("https")
                    .host("maps.googleapis.com")
                    .addPathSegment("maps")
                    .addPathSegment("api")
                    .addPathSegment("distancematrix")
                    .addPathSegment("json")
                    .addQueryParameter("origins", destinations)
                    .addQueryParameter("destinations", destinations)
                    .addQueryParameter("mode", TRAVEL_MODE)
                    .addQueryParameter("language", DISTANCE_LANGUAGE)
                    .addQueryParameter("key", API_KEY)
                    .build();
        }

        private String getRequest(OkHttpClient client, HttpUrl url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }

        private void parseResponse(String response) throws JSONException {
            JSONObject element = new JSONObject(response).getJSONArray("rows")
                    .getJSONObject(1).getJSONArray("elements").getJSONObject(0);
            distance = element.getJSONObject("distance").getString("text");
            duration = element.getJSONObject("duration").getString("text");
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                return getRequest(new OkHttpClient(), getURL());
            } catch (IOException ex) {
                return "Error";
            }
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            progressBar.setVisibility(View.INVISIBLE);
            try {
                parseResponse(response);
                distanceView.setText(distance);
                durationView.setText(duration);
                distanceView.setVisibility(View.VISIBLE);
                durationView.setVisibility(View.VISIBLE);
            } catch (JSONException e) {
                showMessage(MapsActivity.this, ERROR_GET_DISTANCE);
            }
        }
    }
}
