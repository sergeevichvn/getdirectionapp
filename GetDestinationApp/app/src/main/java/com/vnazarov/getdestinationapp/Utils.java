package com.vnazarov.getdestinationapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import static android.graphics.Bitmap.Config.ALPHA_8;

/**
 * Created by vnazarov on 12.12.2016.
 */

public class Utils {

    /*
        Метод принимает на вход id ресурса типа drawable
        возвращает обьект класса BitmapDescriptor
        @param activity
        @param id
        @return BitmapDescriptor
     */
    public static BitmapDescriptor getIconFromResource(Activity activity, int id){
        Drawable drawable = ContextCompat.getDrawable(activity.getApplicationContext(), id);
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), ALPHA_8);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static void showMessage(Activity activity, String msg) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
    }

    /*
        Проверяем включен ли GPS
        @param activity
        @return boolean
     */
    public static boolean getGPSStatus(Activity activity){
            LocationManager locationManager = (LocationManager) activity.getApplicationContext()
                    .getSystemService(Context.LOCATION_SERVICE);
            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
}
